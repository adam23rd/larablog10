<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            
            'body' => [
                'required',
                'string'
            ],
            
        ])->validate();

        $params['comment'] = $request['body'];
        $params['post_id'] = $request['post_id'];
        Auth::user()->comments()->create($params);
            
        return redirect()->back()->with('message','Post created Succesfully ');
    }
}

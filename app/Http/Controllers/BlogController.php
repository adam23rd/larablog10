<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Post;
use App\Models\Category;
use App\Models\Like;
use Inertia\Inertia;

class BlogController extends Controller
{
    public function __construct()
    {
        Inertia::setRootView('blog');
    }

    public function index() 
    {
        $post = Post::activePost()->with('user:id,name')->with('categories:slug,name')->orderBy('id','desc')->get();
        $categories = Category::orderBy('name','asc')->get();
        
        // return [
        //     'posts' => $post,
        //     'categories' => $categories
        // ];
        return Inertia::render('Blog/Index',[
            'posts' => $post,
            'categories' => $categories
        ]);
    }

    public function show($slug)
    {
        $post = Post::activePost()
            ->with('user:id,name')
            ->with('comments',function($q){
                $q->orderBy('id', 'desc');
            })
            ->with('comments.user:id,name','comments.replies.user:id,name')
            ->with('categories:slug,name')
            ->with('tags:slug,name')
            ->where('slug', $slug)->firstOrFail();
        
            $categories = Category::orderBy('name','asc')->get();

        // return ['post' => $post,
        // 'categories' => $categories,
        // 'nextPost' => $post->next_post,
        // 'prevPost' => $post->prev_post];

        $likes = Like::where('post_id',$post->id)->count();
        //dd($like);
        return Inertia::render('Blog/Show', [
            'post' => $post,
            'likes' => $likes,
            'categories' => $categories,
            'nextPost' => $post->next_post,
            'prevPost' => $post->prev_post,
        ]);
    }

    public function user($userId)
    {
        $posts = Post::activePost()
            ->with('user:id,name')
            ->with('categories:slug,name')
            ->where('user_id', $userId)->get();
        
        $categories = Category::orderBy('name','asc')->get();

        return Inertia::render('Blog/Index',[
            'posts' => $posts,
            'categories' => $categories,
        ]);
        
    }

    public function category($slug)
    {
        $posts = Post::activePost()
        ->with('user:id,name')
        ->with('categories:slug,name')
        ->whereHas('categories', function(Builder $query) use ($slug){
            $query->where('slug', $slug);
        })->get();

        $categories = Category::orderBy('name','asc')->get();

        return Inertia::render('Blog/Index',[
            'posts' => $posts,
            'categories' => $categories,
        ]);
    }

    public function tag($slug) 
    {
        $post = Post::activePost()
            ->with('user:id,name')
            ->with('categories:slug,name')
            ->whereHas('tags', function(Builder $query) use ($slug){
                $query->where('slug', $slug);
            })->get();

        $categories = Category::orderBy('name','asc')->get();

        return Inertia::render('Blog/Index',[
            'posts' => $posts,
            'categories' => $categories,
        ]);
    }
}

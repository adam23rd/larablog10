## About Larablog

Larablog
Larablog is a Laravel application that utilizes the Laravel, Vue.js, Inertia, and Tailwind stack. The main features of this application include CRUD Service Blog, Authentication, Comments, Likes, and post/tag sorting. The application is already equipped with migrations and factories for testing purposes.

### Design System

-   Database (can be seen in the images file - DB-Larablog.png
-   Patterns:
    -   Laravel 10 & Vue 3
    -   Monolith x Inertia
    -   Frontend: Vue.js with Tailwind
    -   Accessor and mutator in Laravel
    -   Pagination in Inertia
    -   Inertia is used for the consideration of SEO and development speed. All accesses are rendered server-side but still capable of delivering SPA technology.

Features:

-   Users can perform authentication.
-   Users have access levels (admin, user).
-   Users can create and update posts according to their access rights (admin can update all, users have limited access).
-   Posts can have multiple categories and tags.
-   Posts can have multiple images, comments, and likes.
-   Comment input and editing.
-   Like input."

### Design System

-   php artisan migrate
-   npm install
-   php artisan serve
-   npm run dev
-   php artisan tinker (for dummy)
-   create account (register)
-   login
-   create several post blog
-   add name, category, tags
-   back to home
-   see blogs and details
-   make comments
-   make likes
